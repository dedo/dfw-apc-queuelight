This project will build an AIR Native Extension (ANE).
The purpose of this ANE is to allow a desktop AIR app running on Windows to control a Kuando Busylight for availability notification. 

## Setup for building the ANE: ##
You build the ANE by using Apache Ant to run the buildscript located at `source/QueueLight-native/ant-ane.xml`, but there's some setup you need to do first...

1. install Microsoft Visual Studio (Express Desktop) 
2. Create a file called `build.properties` in the same directory as the buildscript.  In this properties file, you must define overrides for the values in the "MACHINE SPECIFIC PROPERTIES" section of the buildscript.  

## Setup for making code changes in Flash Builder##
1. In Flash Builder, create a new Flex Library Project, choosing `[repo]/source/QueueLightANE-default` as the location.
2. In the "Flex Library Build Path" section of the project properties for the library project you just created, go to the "Source Path" tab and add `[repo]/source/src-common-as` as a source folder
3. Create a new Flex Proejct, choosing `[repo]/source/QueueLightANE-native` as the location.  Choose QueueLightTest.mxml as the main application file. 
4. In the "Flex Library Build Path" section of the project properties for the Flex project you just created, go to the "Source Path" tab and add `[repo]/source/src-common-as` as a source folder
5. Also in the "Flex Library Build Path" section, go to the "Native Extensions" tab, and add "ane-bin" as a folder.
6. in the "Flex Compiler" section, make sure your Flex SDK is [Dedo Flex sdk](https://bitbucket.org/dedo/dedo-sdks.git) 3.5, and add the following compiler argument: `-swf-version=17`

When adding new functions to the API of the ANE, first, define an actionscript-only implementation of the function in `[repo]/source/WaveANE-default/src/com/precocityllc/dfw/apc/queuelight/QueueLightDefault`  This can be just a simple stub, or it can return mock data, or whatever, but it needs to exist so that people on other platforms (i.e. Mac) can still run the app and test the parts that aren't dependent on the fingerprint scanner. 
Once you've provided the default implementation, override that method in `[repo]/source/QueueLightANE-native/src/com/precocityllc/dfw/apc/queuelight/QueueLight.as` with the real implementation that calls into native code.

NOTE:
The Busylight SDK Setup.msi must be ran prior to being able to utilize the Busylight. A install/uninstall batch script file for silent installs/uninstalls is provided under the Scripts folder

[Download BusylightSDK](http://www.plenom.com/support/develop/)