echo "Uninstalling BusylightSDK"
msiexec /i "%~dp0\Busylight SDK Setup.msi" CMDLINE="MODIFY=FALSE REMOVE=TRUE SILENT=TRUE" /quiet
echo "Finished Uninstalling BusylightSDK"