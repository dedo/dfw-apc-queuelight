package com.precocityllc.dfw.apc.queuelight
{
	import com.precocityllc.dfw.apc.queuelight.enum.QueueLightErrorCode;
	import com.precocityllc.dfw.apc.queuelight.events.QueueLightEvent;
	import flash.events.ErrorEvent;
	import flash.events.StatusEvent;
	import flash.external.ExtensionContext;
	
	/////////////////////////////////////////////////////////
	// This is the native implementation
	/////////////////////////////////////////////////////////
	public class QueueLight extends QueueLightDefault
	{
		private var _context:ExtensionContext;
		private var _result:Object;
		private var _shouldTestErrorHandling:Boolean;
		
		public function QueueLight()
		{
			super();
		}
		
		/** @inheritDoc **/
		override public function initANE():Boolean
		{
			if(!_aneInitialized)
			{
				_queueLightInitialized = false;
				try
				{
					_context ||= ExtensionContext.createExtensionContext("com.precocityllc.dfw.apc.queuelight", "");
				}
				catch(error:Error)
				{
					log(error);
				}
				
				if(!_context)
				{
					dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, true, this+"unable to initialize extension context") );
					return false;
				}
				
				_aneInitialized = true;
				_context.addEventListener(StatusEvent.STATUS, _onANEEvent, false, 0, true);
			}
			
			return _aneInitialized;
		}
		
		/**
		 * Initializes the ANE extension context and also will try to intialize the busylight sdk
		 * @return Boolean if succeeded
		 * 
		 */		
		override public function initLight():Boolean
		{
			initANE();
			_result = _context.call("initLight");
			
			if(_result is Error)
			{
				_onError( _result as Error, "initLight");
				_queueLightInitialized = false;
				return false;
			}
			if(_result is Boolean != true)
			{
				_onUnexpectedType(_result, "initLight");
				_queueLightInitialized = false;
				return false;
			}
			_queueLightInitialized = _result as Boolean;
			return _queueLightInitialized;
		}
		
		//calls the terminate function of the QueueLightDLL turning light off	
		override public function quitLight():Boolean
		{
			_result = _context.call("quitLight");
			if(_result is Error)
			{
				_onError(_result as Error, "quitLight");
				return false;
			}
			if(_result is Boolean != true)
			{
				_onUnexpectedType(_result, "quitLight");
				return false;
			}
			_queueLightInitialized = false;
			return _result as Boolean;
		}
		
		override public function setSensitivity(sensitivity:int):Boolean 
		{
			_result = _context.call("setSensitivity",sensitivity);
			if(_result is Error)
			{
				_onError(_result as Error, "setSensitivity");
				return false;
			}
			if(_result is Boolean != true)
			{
				_onUnexpectedType(_result, "setSensitivity");
				return false;
			}
			
			return _result as Boolean;
		}
		/**
		 * Represents the amount of time that the busylight should stay lit. Default is currently set to 0 which is to never turn off 
		 * @param timeout
		 * @return 
		 * 
		 */		
		override public function setTimeout(timeout:int):Boolean 
		{
			_result = _context.call("setTimeout",timeout);
			if(_result is Error)
			{
				_onError(_result as Error, "setTimeout");
				return false;
			}
			if(_result is Boolean != true)
			{
				_onUnexpectedType(_result, "setTimeout");
				return false;
			}
			
			return _result as Boolean;
		}
		
		/** @inheritDoc **/
		override public function dispose():void
		{
			_context.removeEventListener(StatusEvent.STATUS, _onANEEvent);
			_context.dispose();
		}
		
		/**
		 * Once the light has been initialized and is ready to go this function 
		 * should be used to display the specified color
		 * 
		 * @param red 0-255
		 * @param green 0-255
		 * @param blue 0-255
		 * @return Boolean if succeeded
		 * 
		 */
		override public function setStatus(red:int, green:int, blue:int):Boolean
		{
			//check to see if the Busylight device is connected or if the light could be initialized
			//if the device couldn't be found or initialized then lets log it
			
			//TODO
			//ADD check to see if busylight is connected in native code instead
			if(!_isConnected)
			{
				_onError(new Error("Busylight device not connected. Unable to send light command. ", QueueLightErrorCode.LIGHTING_FAILURE),"setStatus");
				return false;
			}
			
			_result = _context.call("setStatus",red, green, blue);
			if(_result is Error)
			{
				_onError(_result as Error, "setStatus");
				return false;
			}
			if(_result is Boolean != true)
			{
				_onUnexpectedType(_result, "setStatus");
				return false;
			}
			return _result as Boolean;
		}
		
		override public function setPulse(red:int, green:int, blue:int):Boolean
		{
			if(!_isConnected)
			{
				_onError(new Error("Busylight device not connected. Unable to send pulse command. ", QueueLightErrorCode.PULSE_FAILURE), "setPulse");
				return false;
			}
			_result = _context.call("setPulse",red, green, blue);
			if(_result is Error)
			{
				_onError(_result as Error, "setPulse");
				return false;
			}
			if(_result is Boolean != true)
			{
				_onUnexpectedType(_result, "setPulse");
				return false;
			}
			return _result as Boolean;
		}
		
		/**
		 * 
		 * Returns true/false if BusyLight Device is connected
		 * @return Boolean
		 * 
		 */		
		override public function isConnected():Boolean
		{
			var isConnectedResult:Object = _context.call("isConnected");
			if(isConnectedResult is Error)
			{
				_onError(isConnectedResult as Error, "isConnected");
				return false;
			}
			if(isConnectedResult is Boolean != true)
			{
				_onUnexpectedType(isConnectedResult, "isConnected");
				return false;
			}
			
			_isConnected = isConnectedResult as Boolean;
			return _isConnected;
		}
		
		/**
		 * 
		 * Test function to see if we can call into native code
		 * @return String
		 * 
		 */		
		override public function sayHello():String
		{
			_result = _context.call("sayHello");
			if(_result is Error)
			{
				_onError(_result as Error, "sayHello");
				return "Error";
			}
			else if(_result is String != true) 
			{
				_onUnexpectedType(_result, "sayHello");
			}
			return _result as String;
		}
		
		override public function testErrorHandling(errorCode:int):Boolean
		{
			_result = _context.call("testErrorHandling", errorCode);
			
			if(_result is Error)
			{
				_onError(_result as Error, "testErrorHandling");
				return false;
			}
			if(_result is Boolean != true)
			{
				_onUnexpectedType(_result, "testErrorHandling");
				return false;
			}
			
			return _result as Boolean;
			
		}
		
		//-----------------------Error and warning message handling-------------------------------------------------
		
		private function _onError(error:Error, funcName:String=null):void
		{
			log("ERROR: "+this+"."+funcName+"() returned error: "+error.message + " w/ ID: " + error.errorID);
			dispatchEvent(new ErrorEvent(ErrorEvent.ERROR, false, false, error.message, error.errorID ) );
		}
		
		private function _onUnexpectedType(result:Object, funcName:String=null):void
		{
			log("WARNING: "+this+"."+funcName+"() returned unexpected value: "+result);
			dispatchEvent(new ErrorEvent(
				ErrorEvent.ERROR, 
				false, 
				false, 
				"WARNING: "+this+"."+funcName+"() returned unexpected value: "+result) 
			);
		}
		
		protected function _onANEEvent(event:StatusEvent):void
		{
			switch(event.code)
			{
				case "busyLightConnected":
					_isConnected = true;
					dispatchEvent( new QueueLightEvent(	QueueLightEvent.BUSYLIGHT_CHANGED, QueueLightEvent.BUSYLIGHT_CONNECTED));
					break;
				case "busyLightDisconnected":
					_isConnected  = false;
					dispatchEvent( new QueueLightEvent(	QueueLightEvent.BUSYLIGHT_CHANGED,QueueLightEvent.BUSYLIGHT_NOT_CONNECTED));
					break;
				case "busyLightChangedError":
					dispatchEvent( new QueueLightEvent(	QueueLightEvent.BUSYLIGHT_CHANGED,QueueLightEvent.BUSYLIGHT_ERROR));
					break;
			}
		}
	}
}