package com.precocityllc.dfw.apc.queuelight.events
{
	import flash.events.Event;
	
	public class QueueLightEvent extends Event
	{
		public static const BUSYLIGHT_CHANGED:String = "busyLightChanged";
		public static const BUSYLIGHT_CONNECTED:int = 7179;
		public static const BUSYLIGHT_NOT_CONNECTED:int = -7179;
		public static const BUSYLIGHT_ERROR:int = -7180;
		private var _code:int;
		
		public function QueueLightEvent(type:String, code:int, bubbles:Boolean = false, cancelable:Boolean = false)
		{
			super(type, bubbles, cancelable);
			_code = code;
		}

		public function get code():int
		{return _code;}

		override public function clone():Event
		{
			return new QueueLightEvent(type, _code, bubbles, cancelable);
		}
		override public function toString():String
		{
			return formatToString("QueueLightEvent", "type", "bubbles", "cancelable",
				"eventPhase");
		}
	}
}