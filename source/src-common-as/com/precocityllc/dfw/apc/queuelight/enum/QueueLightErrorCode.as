package com.precocityllc.dfw.apc.queuelight.enum
{
	public class QueueLightErrorCode
	{
		public static const NO_ERROR_OCCURRED:int = 0;	
		public static const INIT_SDK_FAILURE:int = -7170;
		public static const INIT_COLOR_FAILURE:int = -7171;	
		public static const SET_COLOR_FAILURE:int = -7172;	
		public static const LIGHTING_FAILURE:int = -7173;	
		public static const INVALID_ARGUMENT:int = -7174;	
		public static const PULSE_FAILURE:int = -7175;	
		public static const QUIT_FAILURE:int = -7176;	
		public static const SET_TIMEOUT_FAILURE:int = -7177;	
		public static const IS_CONNECTED_FAILURE:int = -7178;	
		
		public static const codeToName:Object = {
			(NO_ERROR_OCCURRED.valueOf()):"NO_ERROR_OCCURRED",
			(INIT_SDK_FAILURE.valueOf()):"INIT_SDK_FAILURE",
			(INIT_COLOR_FAILURE.valueOf()):"INIT_COLOR_FAILURE",
			(SET_COLOR_FAILURE.valueOf()):"SET_COLOR_FAILURE",
			(LIGHTING_FAILURE.valueOf()):"LIGHTING_FAILURE",
			(PULSE_FAILURE.valueOf()):"PULSE_FAILURE",
			(QUIT_FAILURE.valueOf()):"QUIT_FAILURE",
			(SET_TIMEOUT_FAILURE.valueOf()):"SET_TIMEOUT_FAILURE",
			(IS_CONNECTED_FAILURE.valueOf()):"IS_CONNECTED_FAILURE",	
			(INVALID_ARGUMENT.valueOf()):"INVALID_ARGUMENT"	
		};
	}
}