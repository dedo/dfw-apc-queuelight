package com.precocityllc.dfw.apc.queuelight
{
	import flash.events.EventDispatcher;
	
	
	////////////////////////////////////////////////////////////////////
	// This is the base class for both the native and actionscript-only 
	// versions of the QueueLight class.  Here is where you should
	// add the code for platforms on which you are not providing native 
	// support.  Then make sure to override that in the native version of
	// QueueLight
	////////////////////////////////////////////////////////////////////
	
	[Event(type="flash.events.ErrorEvent", name="error") ]
	[Event(name="busyLightChanged", type="com.precocityllc.dfw.apc.queuelight.events.QueueLightEvent")]
	public class QueueLightDefault extends EventDispatcher
	{
		
		public static const ANE_ERROR:int = -6360;
		public static const UNEXPECTED_RESULT:int = 6360;
		protected var _aneInitialized:Boolean = false;
		protected var _queueLightInitialized:Boolean = false;
		protected var _isConnected:Boolean = false;		
		public var debugTraces:Boolean = true;
		
		public function QueueLightDefault()
		{
			super();
		}
		
		/* Initialize the native extension. MUST call this first. */
		public function initANE():Boolean
		{
			return false;
		}
		
		public function get aneInitialized():Boolean
		{
			return _aneInitialized;
		}
		
		public function get queueLightInitialized():Boolean
		{
			return _queueLightInitialized;
		}
		
		public function get queueLightConnected():Boolean
		{
			return _isConnected;
		}
		
		//
		// ------- Light Control Functions ------- 
		//
		
		public function isConnected():Boolean
		{
			return false;
		}
		
		public function setStatus(red:int, green:int, blue:int):Boolean { return false;}
		
		public function setPulse(red:int, green:int, blue:int):Boolean { return false;}
		
		public function setSensitivity(sensitivity:int):Boolean { return false;}
		
		public function setTimeout(timeout:int):Boolean { return false;}
		
		public function quitLight():Boolean {
			return false;
		}
		
		public function initLight():Boolean
		{
			return false;
		}
		
		public function sayHello():String
		{
			return "hello from actionscript";
		}
		
		protected function log(msg:*):void
		{
			if(debugTraces){trace(msg);}
		}
		
		public function testErrorHandling(errorCode:int):Boolean{ return false; }
		
		public function dispose():void {}
	}
}