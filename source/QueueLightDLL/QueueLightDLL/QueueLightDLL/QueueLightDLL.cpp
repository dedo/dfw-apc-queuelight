#include "QueueLightDLL.h"

using namespace System;
using namespace System::IO;
using namespace System::Runtime::InteropServices;

enum ErrorCode {
	INIT_SDK_FAILURE = -7170,
	INIT_COLOR_FAILURE = -7171,
	SET_COLOR_FAILURE = -7172,
	LIGHTING_FAILURE = -7173,
	INVALID_ARGUMENT = -7174,
	PULSE_FAILURE = -7175,
	QUIT_FAILURE = -7176,
	SET_TIMEOUT_FAILURE = -7177,
	IS_CONNECTED_FAILURE = -7178
};

void initBusylightSDK()
{
	BusyLightController^ busyLight = BusyLightController::Instance;

	if (busyLight->busyLightSDK == nullptr)
	{
		busyLight->errorCode = ErrorCode::INIT_SDK_FAILURE;
		busyLight->busyLightSDK = gcnew SDK();
		//make sure the light is on, bug I think in the busylight sdk
		busyLight->busyLightSDK = gcnew SDK();
	}
}

void initBusylightColor()
{
	BusyLightController^ busyLight = BusyLightController::Instance;
	busyLight->errorCode = ErrorCode::INIT_COLOR_FAILURE;

	if (busyLight->busyLightColor == nullptr)
		busyLight->busyLightColor = gcnew BusylightColor();

	if (busyLight->busyLightSDK != nullptr && busyLight->busyLightColor != nullptr)
	{
		busyLight->busyLightColor = busyLight->busyLightColor->Off;
		busyLight->errorCode = ErrorCode::LIGHTING_FAILURE;
		busyLight->busyLightSDK->Light(busyLight->busyLightColor);

		//set default timeout to never to turn off current light color
		busyLight->busyLightSDK->DPI_Timeout = 0;
	}
}

void handleBusyLightChanged(System::Object^  sender, System::EventArgs^  e) {
	BusyLightController^ busyLight = BusyLightController::Instance;

	try
	{
		Busylight::SDK^ s = safe_cast<Busylight::SDK^>(sender);

		if (s != nullptr)
		{
			if (s->IsLightSupported && (s->GetAttachedBusylightDeviceList()->Count > 0))
			{
				FREDispatchStatusEventAsync(busyLight->ctx, (const uint8_t*)"busyLightConnected", (const uint8_t*)"");
			}
			else
			{
				FREDispatchStatusEventAsync(busyLight->ctx, (const uint8_t*)"busyLightDisconnected", (const uint8_t*)"");
			}
			delete s;
		}
	}
	catch (Exception ^ex)
	{
		if (busyLight->ctx != nullptr)
			FREDispatchStatusEventAsync(busyLight->ctx, (const uint8_t*)"busyLightChangedError", (const uint8_t*)"");
	}
}

void initEventListeners()
{
	BusyLightController^ busyLight = BusyLightController::Instance;

	if (busyLight->busyLightSDK != nullptr)
		busyLight->busyLightSDK->BusyLightChanged += gcnew System::EventHandler(&handleBusyLightChanged);
}

// Sets the current busyLight color given the r,g,b values
// argv[0] = red
// argv[1] = green
// argv[2] = blue
bool setColor(FREObject argv[], FREObject &ret, BusylightColor^ col = nullptr)
{
	FREResult res;
	bool result = false;
	int red, green, blue = 0;
	BusyLightController^ busyLight = BusyLightController::Instance;
	busyLight->errorCode = ErrorCode::SET_COLOR_FAILURE;

	if (col == nullptr)
	{
		res = FREGetObjectAsInt32(argv[0], &red);
		if (res != FRE_OK) { ret = FRENewError("Error while trying to parse RGB value", ErrorCode::INVALID_ARGUMENT); }
		res = FREGetObjectAsInt32(argv[1], &green);
		if (res != FRE_OK) { ret = FRENewError("Error while trying to parse RGB value", ErrorCode::INVALID_ARGUMENT); }
		res = FREGetObjectAsInt32(argv[2], &blue);
		if (res != FRE_OK) { ret = FRENewError("Error while trying to parse RGB value", ErrorCode::INVALID_ARGUMENT); }
	}
	else {
		red = col->RedRgbValue;
		blue = col->BlueRgbValue;
		green = col->GreenRgbValue;
	}

	if (busyLight->busyLightColor != nullptr)
	{
		busyLight->busyLightColor->BlueRgbValue = blue;
		busyLight->busyLightColor->GreenRgbValue = green;
		busyLight->busyLightColor->RedRgbValue = red;
		result = true;
	}

	return result;
}

//initialize light
FREObject initLight(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	FREObject ret;
	FRENewObjectFromBool(false, &ret);
	BusyLightController^ busyLight = BusyLightController::Instance;
	try
	{
		if (busyLight->errorCodeToCheck != ErrorCode::INIT_SDK_FAILURE && !busyLight->shouldErrorTest)
			initBusylightSDK();
		else {
			busyLight->busyLightSDK = nullptr;
			busyLight->busyLightSDK->ToString();
		}

		if (!busyLight->shouldErrorTest)
			initEventListeners();

		if (busyLight->errorCodeToCheck != ErrorCode::INIT_COLOR_FAILURE && !busyLight->shouldErrorTest)
			initBusylightColor();
		else {
			busyLight->busyLightColor = nullptr;
			busyLight->busyLightColor->ToString();
		}
		
		FRENewObjectFromBool(busyLight->busyLightSDK != nullptr && busyLight->busyLightColor != nullptr, &ret);
	}
	catch (Exception ^ex)
	{
		return FRENewError((char*)(void*)Marshal::StringToHGlobalAnsi(ex->Message), busyLight->errorCode);
	}

	return ret;
}

FREObject isConnected(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	BusyLightController^ busyLight = BusyLightController::Instance;
	FREObject ret;
	FREResult res;
	FRENewObjectFromBool(false, &ret);

	try {
		busyLight->errorCode = ErrorCode::IS_CONNECTED_FAILURE;
		if (busyLight->shouldErrorTest)
			busyLight->busyLightSDK = nullptr;
		FRENewObjectFromBool(busyLight->busyLightSDK->IsLightSupported && busyLight->busyLightSDK->GetAttachedBusylightDeviceList()->Count > 0, &ret);
		return ret;
	}
	catch (Exception ^ex)
	{
		return FRENewError((char*)(void*)Marshal::StringToHGlobalAnsi("Error while trying to check connectivity of Busylight: " + ex->Message), busyLight->errorCode);
	}
}

// return a string 'Hello from C++' as a test 
FREObject sayHello(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	FREObject ret;
	FRENewObjectFromUTF8(14, (const uint8_t*)"Hello from C++", &ret);
	return ret;
}

// Sets the Sensitivity of light
// argv[0] = Senstivity level 0,2,4,8,16
FREObject setSensitivity(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	BusyLightController^ busyLight = BusyLightController::Instance;
	FREObject ret;
	FREResult res;
	FRENewObjectFromBool(false, &ret);

	try {
		int sens;
		res = FREGetObjectAsInt32(argv[0], &sens);
		if (res != FRE_OK) { return FRENewError("setSensitivityType: could not retrieve sensitivity value", ErrorCode::INVALID_ARGUMENT); }
		busyLight->busyLightSDK->DPI_Sensivity = sens;
		FRENewObjectFromBool(true, &ret);
		return ret;
	}
	catch (Exception ^ex)
	{
		return FRENewError((char*)(void*)Marshal::StringToHGlobalAnsi("Error while trying to set DPI_Sensivity of Busylight: " + ex->Message), busyLight->errorCode);
	}
}

// Sets the timeout value of how long the busylight should stay lit
// argv[0] = time ( 0 if always on which is default) 
FREObject setTimeout(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	BusyLightController^ busyLight = BusyLightController::Instance;
	FREObject ret;
	FREResult res;
	FRENewObjectFromBool(false, &ret);

	try {
		int timeout;
		busyLight->errorCode = ErrorCode::SET_TIMEOUT_FAILURE;
		res = FREGetObjectAsInt32(argv[0], &timeout);
		if (res != FRE_OK) { return FRENewError("setTimeout: could not retrieve timeout value", ErrorCode::INVALID_ARGUMENT); }

		if (busyLight->shouldErrorTest)
			busyLight->busyLightSDK = nullptr;
		busyLight->busyLightSDK->DPI_Timeout = timeout;
		FRENewObjectFromBool(true, &ret);
		return ret;
	}
	catch (Exception ^ex)
	{
		return FRENewError((char*)(void*)Marshal::StringToHGlobalAnsi("Error while trying to set DPI_Timeout of Busylight: " + ex->Message), busyLight->errorCode);
	}
}


// Given a color in r,g,b will pulse or pwm the busylight
// argv[0] = red
// argv[1] = blue
// argv[2] = green
FREObject setPulse(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	BusyLightController^ busyLight = BusyLightController::Instance;
	FREObject ret;
	FRENewObjectFromBool(false, &ret);

	try
	{
		Busylight::PulseSequence^ seq = nullptr;

		if (!busyLight->shouldErrorTest)
		{
			if (!setColor(argv, ret))
				return ret;
			busyLight->errorCode = ErrorCode::PULSE_FAILURE;
			seq = gcnew Busylight::PulseSequence();
			seq->Color = busyLight->busyLightColor;
			seq->Step1 = 8;
			seq->Step2 = 16;
			seq->Step3 = 32;
			seq->Step4 = 64;
			seq->Step5 = 128;
			busyLight->busyLightSDK->Pulse(seq);
			FRENewObjectFromBool(true, &ret);
		}
		else
		{
			//error handling testing
			seq = nullptr;
			busyLight->errorCode = ErrorCode::PULSE_FAILURE;
			busyLight->busyLightSDK->Pulse(seq);
		}
		return ret;
	}
	catch (Exception ^ex)
	{
		return FRENewError((char*)(void*)Marshal::StringToHGlobalAnsi("Error while trying to pulse Busylight: " + ex->Message), busyLight->errorCode);
	}
}

// Will light according to RGB value supplied
// argv[0] = red
// argv[1] = green
// argv[2] = blue

FREObject setStatus(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	BusyLightController^ busyLight = BusyLightController::Instance;
	FREObject ret;
	FRENewObjectFromBool(false, &ret);

	try {
		if (!busyLight->shouldErrorTest)
		{
			if (!setColor(argv, ret))
				return ret;

			//submit color to light
			busyLight->errorCode = ErrorCode::LIGHTING_FAILURE;
			busyLight->busyLightSDK->Light(busyLight->busyLightColor);
			FRENewObjectFromBool(true, &ret);
		}
		else {
			//error handling testing
			setColor(argv, ret);
			//test when color is invalid
			busyLight->busyLightSDK->Light(busyLight->busyLightColor);
		}
		return ret;
	}
	catch (Exception ^ex)
	{
		return FRENewError((char*)(void*)Marshal::StringToHGlobalAnsi("Error while trying to setStautus: " + ex->Message), busyLight->errorCode);
	}
}

// Calls the terminate() function of the BusylightSDK
// turns off the BusyLight
FREObject quitLight(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	BusyLightController^ busyLight = BusyLightController::Instance;
	FREObject ret;
	FRENewObjectFromBool(false, &ret);

	try {
		busyLight->errorCode = ErrorCode::QUIT_FAILURE;
		//if were testing error handling then let's set SDK to empty
		if (busyLight->shouldErrorTest)
			busyLight->busyLightSDK = nullptr;
		busyLight->TerminateLight();
		FRENewObjectFromBool(true, &ret);
		return ret;
	}
	catch (Exception ^ex)
	{
		return FRENewError((char*)(void*)Marshal::StringToHGlobalAnsi("Unable to terminate the Busylight: " + ex->Message), busyLight->errorCode);
	}
}

FREObject testErrorHandling(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
	FREResult res;
	FREObject ret;
	BusyLightController^ busyLight = BusyLightController::Instance;

	FRENewObjectFromBool(false, &ret);
	int32_t errorCodeToCheck = 0;
	int errorCodeCheck;
	res = FREGetObjectAsInt32(argv[0], &errorCodeToCheck);
	if (res == FRE_OK) { errorCodeCheck = errorCodeToCheck; }
	else { return false; }

	busyLight->shouldErrorTest = true;

	switch (errorCodeCheck)
	{
	case ErrorCode::INIT_COLOR_FAILURE:
		return initLight(ctx, funcData, argc, argv);
		break;
	case ErrorCode::INIT_SDK_FAILURE:
		return initLight(ctx, funcData, argc, argv);
		break;
	case ErrorCode::INVALID_ARGUMENT:
		if (argc >= 1)
			argv[0] = nullptr;
		return setStatus(ctx, funcData, argc, argv);
		break;
	case ErrorCode::IS_CONNECTED_FAILURE:
		return isConnected(ctx, funcData, argc, argv);
		break;
	case ErrorCode::LIGHTING_FAILURE:
		return isConnected(ctx, funcData, argc, argv);
		break;
	case ErrorCode::PULSE_FAILURE:
		return setPulse(ctx, funcData, argc, argv);
		break;
	case ErrorCode::QUIT_FAILURE:
		return quitLight(ctx, funcData, argc, argv);
		break;
	case ErrorCode::SET_COLOR_FAILURE:
		if (argc >= 1)
			argv[0] = nullptr;
		return setStatus(ctx, funcData, argc, argv);
		break;
	case ErrorCode::SET_TIMEOUT_FAILURE:
		return setTimeout(ctx, funcData, argc, argv);
		break;
	default:
		return ret;
	}
}

//////////////////////////////////////
//   Native Extension initialization
/////////////////////////////////////
void contextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctions, const FRENamedFunction** functions)
{
	*numFunctions = 9;
	FRENamedFunction* func = new FRENamedFunction[*numFunctions];

	func[0].name = (const uint8_t*) "sayHello";
	func[0].functionData = NULL;
	func[0].function = &sayHello;

	func[1].name = (const uint8_t*) "initLight";
	func[1].functionData = NULL;
	func[1].function = &initLight;

	func[2].name = (const uint8_t*) "setStatus";
	func[2].functionData = NULL;
	func[2].function = &setStatus;

	func[3].name = (const uint8_t*) "isConnected";
	func[3].functionData = NULL;
	func[3].function = &isConnected;

	func[4].name = (const uint8_t*) "quitLight";
	func[4].functionData = NULL;
	func[4].function = &quitLight;

	func[5].name = (const uint8_t*) "setPulse";
	func[5].functionData = NULL;
	func[5].function = &setPulse;

	func[6].name = (const uint8_t*) "setSensitivity";
	func[6].functionData = NULL;
	func[6].function = &setSensitivity;

	func[7].name = (const uint8_t*) "setTimeout";
	func[7].functionData = NULL;
	func[7].function = &setTimeout;

	func[8].name = (const uint8_t*) "testErrorHandling";
	func[8].functionData = NULL;
	func[8].function = &testErrorHandling;

	*functions = func;
}

void contextFinalizer(FREContext ctx)
{
	BusyLightController^ busyLight = BusyLightController::Instance;

	//Clean up resources
	if (busyLight != nullptr)
		busyLight->Destruct();

	return;
}

void initializer(void** extData, FREContextInitializer* ctxInitializer, FREContextFinalizer* ctxFinalizer)
{
	*ctxInitializer = &contextInitializer;
	*ctxFinalizer = &contextFinalizer;
}

void finalizer(void* extData)
{
	return;
}