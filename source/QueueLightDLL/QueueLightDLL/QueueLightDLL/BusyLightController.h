#pragma once
using namespace System;
using namespace System::Runtime::InteropServices;
using namespace System::Diagnostics;
using namespace Busylight;

ref class BusyLightController
{
private:
	static BusyLightController _instance;
	BusyLightController() {}
public:
	static property BusyLightController^ Instance{BusyLightController^ get() { return %_instance; }}
	static int errorCode;
	static int errorCodeToCheck;
	static bool shouldErrorTest;

	BusylightColor^ busyLightColor = nullptr;
	SDK^ busyLightSDK = nullptr;
	FREContext ctx;

	void Destruct()
	{
		if (busyLightSDK != nullptr)
		{
			delete busyLightSDK;
			busyLightSDK = nullptr;
		}
		if (busyLightColor != nullptr)
		{
			delete busyLightColor;
			busyLightColor = nullptr;
		}
	}

	void TerminateLight()
	{
		BusyLightController^ busyLight = BusyLightController::Instance;

		if (busyLight != nullptr && busyLight->busyLightSDK != nullptr)
		{
			busyLight->busyLightSDK->Terminate();
		}
	}
};