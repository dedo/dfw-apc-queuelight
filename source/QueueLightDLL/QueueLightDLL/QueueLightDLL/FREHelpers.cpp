/**
* some convenience functions for dealing with FREObjects
*
* @author Doug McCluer
*/

#include <cstdlib>
#include <errno.h>
#include "FREHelpers.h"
#include <string.h>


const uint8_t* getFREObjectTypeName(FREObject object)
{
	const uint8_t *typeName;
	FREObjectType objectType;
	FREGetObjectType(object, &objectType);
	switch (objectType)
	{
	case FRE_TYPE_ARRAY:
		typeName = (const uint8_t*)"Array";
		break;
	case FRE_TYPE_BITMAPDATA:
		typeName = (const uint8_t*)"BitmapData";
		break;
	case FRE_TYPE_BOOLEAN:
		typeName = (const uint8_t*)"Boolean";
		break;
	case FRE_TYPE_BYTEARRAY:
		typeName = (const uint8_t*)"ByteArray";
		break;
	case FRE_TYPE_NULL:
		typeName = (const uint8_t*)"null";
		break;
	case FRE_TYPE_NUMBER:
		typeName = (const uint8_t*)"Number";
		break;
	case FRE_TYPE_OBJECT:
		typeName = (const uint8_t*)"Object";
		break;
	case FRE_TYPE_STRING:
		typeName = (const uint8_t*)"String";
		break;
	case FRE_TYPE_VECTOR:
		typeName = (const uint8_t*)"Vector";
		break;
	default:
		typeName = (const uint8_t*)"Unknown";
		break;
	}
	return typeName;
}

FREObject getFREString(const char* str)
{
	FREObject returnval;
	FRENewObjectFromUTF8(strlen(str), (const uint8_t*)str, &returnval);
	return returnval;

}

FREResult FRENewDate(FREObject* obj, FREObject* exception,
	uint32_t year, uint32_t month, uint32_t date, uint32_t hours,
	uint32_t minutes, uint32_t seconds, uint32_t milliseconds)
{
	FREObject args[7];
	FRENewObjectFromUint32(year, &args[0]);
	FRENewObjectFromUint32(month, &args[1]);
	FRENewObjectFromUint32(date, &args[2]);
	FRENewObjectFromUint32(hours, &args[3]);
	FRENewObjectFromUint32(minutes, &args[4]);
	FRENewObjectFromUint32(seconds, &args[5]);
	FRENewObjectFromUint32(milliseconds, &args[6]);

	return FRENewObject((const uint8_t*)"Date", 7, args, obj, exception);
}

bool FRENewObjectFromLPWSTR(const wchar_t* str, FREObject* obj)
{
	FREResult res;
	size_t len = wcslen(str);
	size_t numChars;

	size_t BUFFERSIZE = len * sizeof(char) + 1;
	char* val = new char[BUFFERSIZE];
	errno_t err = wcstombs_s(&numChars, val, BUFFERSIZE, (const wchar_t*)str, len);

	//if the string is all 8-bit characters (most likely case in English locale), 
	//then the first call should succeed, but if any of the characters take up more 
	//than one byte then it will fail and return ERANGE.  Increase the
	//buffer size and try again until we get a different code
	while (err == ERANGE)
	{
		BUFFERSIZE *= 2;
		delete val;
		val = new char[BUFFERSIZE];
		err = wcstombs_s(&numChars, val, BUFFERSIZE, (const wchar_t*)str, len);
	}

	//if we get some other error code, then fail out
	if (err != 0)
	{
		return false;
	}

	res = FRENewObjectFromUTF8(numChars, (const uint8_t*)val, obj);
	delete[] val;
	return res == FRE_OK;
}

FREObject FRENewError(char* message, int code)
{
	FREObject error, exception;
	FREObject args[2];

	args[0] = getFREString(message);
	FRENewObjectFromInt32(code, &args[1]);
	FREResult res = FRENewObject((const uint8_t*)"Error", 2, args, &error, &exception);
	if (res != FRE_OK) { return exception; }
	return error;
}

char* FREResultToChar(FREResult res)
{
	switch (res)
	{
	case FRE_OK:
		return "FRE_OK";
		break;
	case FRE_NO_SUCH_NAME:
		return "FRE_NO_SUCH_NAME";
		break;
	case FRE_INVALID_OBJECT:
		return "FRE_INVALID_OBJECT";
		break;
	case FRE_ACTIONSCRIPT_ERROR:
		return "FRE_ACTIONSCRIPT_ERROR";
		break;
	case FRE_ILLEGAL_STATE:
		return "FRE_ILLEGAL_STATE";
		break;
	case FRE_WRONG_THREAD:
		return "FRE_WRONG_THREAD";
		break;
	case FRE_READ_ONLY:
		return "FRE_READ_ONLY";
		break;
	case FRE_INVALID_ARGUMENT:
		return "FRE_INVALID_ARGUMENT";
		break;
	case FRE_TYPE_MISMATCH:
		return "FRE_TYPE_MISMATCH";
		break;
	}
	return "UNKNOWN";
}


FREObject FRENewObjectFromFREResult(FREResult res)
{
	return getFREString(FREResultToChar(res));
}
