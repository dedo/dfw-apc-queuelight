#pragma once
#include "stdafx.h"
#include "FlashRuntimeExtensions.h"
#include "FREHelpers.h"
#include "BusyLightController.h"
#include <msclr\auto_gcroot.h>

extern "C"
{
	__declspec(dllexport) void initializer(void** extData, FREContextInitializer* ctxInitializer, FREContextFinalizer* ctxFinalizer);
	__declspec(dllexport) void finalizer(void* extData);
}