#pragma once
#include "FlashRuntimeExtensions.h"
/**
 * some convenience functions for dealing with FREObjects
 *
 * @author Doug McCluer
 */

const uint8_t* getFREObjectTypeName(FREObject object);
FREObject getFREString(const char* str);
char* FREResultToChar(FREResult res);
FREObject FRENewObjectFromFREResult(FREResult res);
FREObject FRENewError(char* message, int code);
bool FRENewObjectFromLPWSTR(const wchar_t* str, FREObject* obj);
FREResult FRENewDate(	FREObject* obj, FREObject* exception,
						uint32_t year=1970, uint32_t month=0, uint32_t date=1, uint32_t hours=0, 
						uint32_t minutes=0, uint32_t seconds=0, uint32_t milliseconds=0 );

