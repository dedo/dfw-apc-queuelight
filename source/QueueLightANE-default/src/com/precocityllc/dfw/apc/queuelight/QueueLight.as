package com.precocityllc.dfw.apc.queuelight
{
	///////////////////////////////////////////////////////////////////
	// This is the default actionscript-only implementation.
	//
	// You should not add any new public members to this class.
	//
	// The only code that should go here are overrides and private members.
	// To add new features to the ANE, add your default code to 
	// com.dedoinc.dfw.apc.queuelight.QueueLightDefault, which
	// can be found in the src-common-as folder.  Then override the default 
	// behaviour in the native implementation and here 
	///////////////////////////////////////////////////////////////////
	public class QueueLight extends QueueLightDefault
	{
		public function QueueLight()
		{
			super();
		}
	}
}